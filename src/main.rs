extern crate clap;
mod elements;
use clap::{Arg, App};
use elements::ElementString;
use elements::Element;
use std::option::Option;

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(Arg::with_name("STRING")
                 .help("String to be chemised")
                 .required(true)
                 .index(1))
        .get_matches();
    build_name(matches.value_of("STRING").unwrap(),
               &mut ElementString(vec![]));
}

fn build_name(slice: &str, elements: &mut ElementString) {
    match slice.len() {
        0 => {
            println!("{:?}", elements);
            elements.0.clear();
        }
        1 => elementise(slice, 1, elements),
        _ => {
            elementise(slice, 2, &mut elements.clone());
            elementise(slice, 1, elements);
        }
    }
}

fn elementise(slice: &str, sub_slice_len: usize, elements: &mut ElementString) {
    if let (Some(el), tail) = slice_to_element(slice, sub_slice_len) {
        {
            elements.0.push(el);
        }
        build_name(tail, elements);
    }
}

fn slice_to_element(string: &str, slice_len: usize) -> (Option<&'static Element>, &str) {
    let (head, tail) = string.split_at(slice_len);
    (find_element(head), tail)
}

fn find_element(element: &str) -> Option<&'static Element> {
    use elements::TABLE;
    TABLE
        .iter()
        .find(|&el| el.symbol.to_lowercase() == element.to_lowercase())
}

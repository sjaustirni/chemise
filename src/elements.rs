use std::vec::Vec;
use std;
pub struct Element {
    pub symbol: &'static str,
    name: &'static str,
}

#[derive(Clone)]
pub struct ElementString(pub Vec<&'static Element>);

impl std::fmt::Debug for Element {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.symbol)
    }
}

impl std::fmt::Debug for ElementString {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut result: String = String::new();
        if !self.0.is_empty() {
            for el in &self.0 {
                result += el.symbol;
            }
            result += " (";
            for el in &self.0 {
                result += el.name;
                result += ", ";
            }
            result.pop();
            result.pop();
            result += ")"
        }
        write!(f, "{}", result)
    }
}

impl std::ops::Deref for ElementString {
    type Target = Vec<&'static Element>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}


pub const TABLE: &'static [Element] = &[Element {
                                            symbol: "H",
                                            name: "Hydrogen",
                                        },
                                        Element {
                                            symbol: "He",
                                            name: "Helium",
                                        },
                                        Element {
                                            symbol: "Li",
                                            name: "Lithium",
                                        },
                                        Element {
                                            symbol: "Be",
                                            name: "Beryllium",
                                        },
                                        Element {
                                            symbol: "B",
                                            name: "Boron",
                                        },
                                        Element {
                                            symbol: "C",
                                            name: "Carbon",
                                        },
                                        Element {
                                            symbol: "N",
                                            name: "Nitrogen",
                                        },
                                        Element {
                                            symbol: "O",
                                            name: "Oxygen",
                                        },
                                        Element {
                                            symbol: "F",
                                            name: "Fluorine",
                                        },
                                        Element {
                                            symbol: "Ne",
                                            name: "Neon",
                                        },
                                        Element {
                                            symbol: "Na",
                                            name: "Sodium",
                                        },
                                        Element {
                                            symbol: "Mg",
                                            name: "Magnesium",
                                        },
                                        Element {
                                            symbol: "Al",
                                            name: "Aluminium",
                                        },
                                        Element {
                                            symbol: "Si",
                                            name: "Silicon",
                                        },
                                        Element {
                                            symbol: "P",
                                            name: "Phosphorus",
                                        },
                                        Element {
                                            symbol: "S",
                                            name: "Sulfur",
                                        },
                                        Element {
                                            symbol: "Cl",
                                            name: "Chlorine",
                                        },
                                        Element {
                                            symbol: "Ar",
                                            name: "Argon",
                                        },
                                        Element {
                                            symbol: "K",
                                            name: "Potassium",
                                        },
                                        Element {
                                            symbol: "Ca",
                                            name: "Calcium",
                                        },
                                        Element {
                                            symbol: "Sc",
                                            name: "Scandium",
                                        },
                                        Element {
                                            symbol: "Ti",
                                            name: "Titanium",
                                        },
                                        Element {
                                            symbol: "V",
                                            name: "Vanadium",
                                        },
                                        Element {
                                            symbol: "Cr",
                                            name: "Chromium",
                                        },
                                        Element {
                                            symbol: "Mn",
                                            name: "Manganese",
                                        },
                                        Element {
                                            symbol: "Fe",
                                            name: "Iron",
                                        },
                                        Element {
                                            symbol: "Co",
                                            name: "Cobalt",
                                        },
                                        Element {
                                            symbol: "Ni",
                                            name: "Nickel",
                                        },
                                        Element {
                                            symbol: "Cu",
                                            name: "Copper",
                                        },
                                        Element {
                                            symbol: "Zn",
                                            name: "Zinc",
                                        },
                                        Element {
                                            symbol: "Ga",
                                            name: "Gallium",
                                        },
                                        Element {
                                            symbol: "Ge",
                                            name: "Germanium",
                                        },
                                        Element {
                                            symbol: "As",
                                            name: "Arsenic",
                                        },
                                        Element {
                                            symbol: "Se",
                                            name: "Selenium",
                                        },
                                        Element {
                                            symbol: "Br",
                                            name: "Bromine",
                                        },
                                        Element {
                                            symbol: "Kr",
                                            name: "Krypton",
                                        },
                                        Element {
                                            symbol: "Rb",
                                            name: "Rubidium",
                                        },
                                        Element {
                                            symbol: "Sr",
                                            name: "Strontium",
                                        },
                                        Element {
                                            symbol: "Y",
                                            name: "Yttrium",
                                        },
                                        Element {
                                            symbol: "Zr",
                                            name: "Zirconium",
                                        },
                                        Element {
                                            symbol: "Nb",
                                            name: "Niobium",
                                        },
                                        Element {
                                            symbol: "Mo",
                                            name: "Molybdenum",
                                        },
                                        Element {
                                            symbol: "Tc",
                                            name: "Technetium",
                                        },
                                        Element {
                                            symbol: "Ru",
                                            name: "Ruthenium",
                                        },
                                        Element {
                                            symbol: "Rh",
                                            name: "Rhodium",
                                        },
                                        Element {
                                            symbol: "Pd",
                                            name: "Palladium",
                                        },
                                        Element {
                                            symbol: "Ag",
                                            name: "Silver",
                                        },
                                        Element {
                                            symbol: "Cd",
                                            name: "Cadmium",
                                        },
                                        Element {
                                            symbol: "In",
                                            name: "Indium",
                                        },
                                        Element {
                                            symbol: "Sn",
                                            name: "Tin",
                                        },
                                        Element {
                                            symbol: "Sb",
                                            name: "Antimony",
                                        },
                                        Element {
                                            symbol: "Te",
                                            name: "Tellurium",
                                        },
                                        Element {
                                            symbol: "I",
                                            name: "Iodine",
                                        },
                                        Element {
                                            symbol: "Xe",
                                            name: "Xenon",
                                        },
                                        Element {
                                            symbol: "Cs",
                                            name: "Caesium",
                                        },
                                        Element {
                                            symbol: "Ba",
                                            name: "Barium",
                                        },
                                        Element {
                                            symbol: "La",
                                            name: "Lanthanum",
                                        },
                                        Element {
                                            symbol: "Ce",
                                            name: "Cerium",
                                        },
                                        Element {
                                            symbol: "Pr",
                                            name: "Praseodymium",
                                        },
                                        Element {
                                            symbol: "Nd",
                                            name: "Neodymium",
                                        },
                                        Element {
                                            symbol: "Pm",
                                            name: "Promethium",
                                        },
                                        Element {
                                            symbol: "Sm",
                                            name: "Samarium",
                                        },
                                        Element {
                                            symbol: "Eu",
                                            name: "Europium",
                                        },
                                        Element {
                                            symbol: "Gd",
                                            name: "Gadolinium",
                                        },
                                        Element {
                                            symbol: "Tb",
                                            name: "Terbium",
                                        },
                                        Element {
                                            symbol: "Dy",
                                            name: "Dysprosium",
                                        },
                                        Element {
                                            symbol: "Ho",
                                            name: "Holmium",
                                        },
                                        Element {
                                            symbol: "Er",
                                            name: "Erbium",
                                        },
                                        Element {
                                            symbol: "Tm",
                                            name: "Thulium",
                                        },
                                        Element {
                                            symbol: "Yb",
                                            name: "Ytterbium",
                                        },
                                        Element {
                                            symbol: "Lu",
                                            name: "Lutetium",
                                        },
                                        Element {
                                            symbol: "Hf",
                                            name: "Hafnium",
                                        },
                                        Element {
                                            symbol: "Ta",
                                            name: "Tantalum",
                                        },
                                        Element {
                                            symbol: "W",
                                            name: "Tungsten",
                                        },
                                        Element {
                                            symbol: "Re",
                                            name: "Rhenium",
                                        },
                                        Element {
                                            symbol: "Os",
                                            name: "Osmium",
                                        },
                                        Element {
                                            symbol: "Ir",
                                            name: "Iridium",
                                        },
                                        Element {
                                            symbol: "Pt",
                                            name: "Platinum",
                                        },
                                        Element {
                                            symbol: "Au",
                                            name: "Gold",
                                        },
                                        Element {
                                            symbol: "Hg",
                                            name: "Mercury",
                                        },
                                        Element {
                                            symbol: "Tl",
                                            name: "Thallium",
                                        },
                                        Element {
                                            symbol: "Pb",
                                            name: "Lead",
                                        },
                                        Element {
                                            symbol: "Bi",
                                            name: "Bismuth",
                                        },
                                        Element {
                                            symbol: "Po",
                                            name: "Polonium",
                                        },
                                        Element {
                                            symbol: "At",
                                            name: "Astatine",
                                        },
                                        Element {
                                            symbol: "Rn",
                                            name: "Radon",
                                        },
                                        Element {
                                            symbol: "Fr",
                                            name: "Francium",
                                        },
                                        Element {
                                            symbol: "Ra",
                                            name: "Radium",
                                        },
                                        Element {
                                            symbol: "Ac",
                                            name: "Actinium",
                                        },
                                        Element {
                                            symbol: "Th",
                                            name: "Thorium",
                                        },
                                        Element {
                                            symbol: "Pa",
                                            name: "Protactinium",
                                        },
                                        Element {
                                            symbol: "U",
                                            name: "Uranium",
                                        },
                                        Element {
                                            symbol: "Np",
                                            name: "Neptunium",
                                        },
                                        Element {
                                            symbol: "Pu",
                                            name: "Plutonium",
                                        },
                                        Element {
                                            symbol: "Am",
                                            name: "Americium",
                                        },
                                        Element {
                                            symbol: "Cm",
                                            name: "Curium",
                                        },
                                        Element {
                                            symbol: "Bk",
                                            name: "Berkelium",
                                        },
                                        Element {
                                            symbol: "Cf",
                                            name: "Californium",
                                        },
                                        Element {
                                            symbol: "Es",
                                            name: "Einsteinium",
                                        },
                                        Element {
                                            symbol: "Fm",
                                            name: "Fermium",
                                        },
                                        Element {
                                            symbol: "Md",
                                            name: "Mendelevium",
                                        },
                                        Element {
                                            symbol: "No",
                                            name: "Nobelium",
                                        },
                                        Element {
                                            symbol: "Lr",
                                            name: "Lawrencium",
                                        },
                                        Element {
                                            symbol: "Rf",
                                            name: "Rutherfordium",
                                        },
                                        Element {
                                            symbol: "Db",
                                            name: "Dubnium",
                                        },
                                        Element {
                                            symbol: "Sg",
                                            name: "Seaborgium",
                                        },
                                        Element {
                                            symbol: "Bh",
                                            name: "Bohrium",
                                        },
                                        Element {
                                            symbol: "Hs",
                                            name: "Hassium",
                                        },
                                        Element {
                                            symbol: "Mt",
                                            name: "Meitnerium",
                                        },
                                        Element {
                                            symbol: "Ds",
                                            name: "Darmstadtium",
                                        },
                                        Element {
                                            symbol: "Rg",
                                            name: "Roentgenium",
                                        },
                                        Element {
                                            symbol: "Cn",
                                            name: "Copernicium",
                                        },
                                        Element {
                                            symbol: "Nh",
                                            name: "Nihonium",
                                        },
                                        Element {
                                            symbol: "Fl",
                                            name: "Flerovium",
                                        },
                                        Element {
                                            symbol: "Mc",
                                            name: "Moscovium",
                                        },
                                        Element {
                                            symbol: "Lv",
                                            name: "Livermorium",
                                        },
                                        Element {
                                            symbol: "Ts",
                                            name: "Tennessine",
                                        },
                                        Element {
                                            symbol: "Og",
                                            name: "Oganesson",
                                        }];
